<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="edu.ifsp.lojinha.modelo.Cliente" %>
<%@ page import="java.util.List" %>
    
<%
List<Cliente> lista = (List<Cliente>)request.getAttribute("lista");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lista de Clientes</title>

<style type="text/css">
tr:nth-child(even){
	background: yellow;
}

</style>

</head>
<body>
<h1>Lista de Clientes</h1>


<table border="1">
<tr><th>Nome</th><th>Email</th></tr>
<% 
	for (Cliente c: lista) {
%>
<tr><td><%= c.getNome() %></td><td><%= c.getEmail() %></td></tr>
<% } %>
</table>

</body>
</html>