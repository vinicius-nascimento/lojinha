package edu.ifsp.lojinha.web;

import java.io.IOException;
import java.util.List;

import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.persistencia.ClienteDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/listar")
public class ListarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClienteDAO dao = new ClienteDAO();
		List<Cliente> lista = dao.listAll();
		
		request.setAttribute("lista", lista);		
		
		RequestDispatcher rd = request.getRequestDispatcher("lista.jsp");
		rd.forward(request, response);		
	}

}
